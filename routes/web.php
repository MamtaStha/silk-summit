<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Contact;

Route::get('/' , 'Frontend\FrontendController@index')->name('index');
Route::get('/about' , 'Frontend\FrontendController@about')->name('about');
Route::get('/sponsor','Frontend\FrontendController@sponsor')->name('sponsor');

Route::get('/contact','Frontend\FrontendController@contact')->name('contact');
Route::get('/speaker','Frontend\FrontendController@speaker')->name('speaker');
Route::get('/schedule','Frontend\FrontendController@schedule')->name('schedule');
Route::get('/venue','Frontend\FrontendController@venue')->name('venue');
Route::get('/workshop','Frontend\FrontendController@workshop')->name('workshop');
Route::get('/challenge','Frontend\FrontendController@challenge')->name('challenge');
Route::get('/comming_soon','Frontend\FrontendController@commingSoon')->name('comming_soon');
Route::post('/store','Frontend\FrontendController@store')->name('store.message');
//View::composer(['*'],function($view){
//    $contact= Contact::first();
//    $view->with(compact('contact'));
//});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
