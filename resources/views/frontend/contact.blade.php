@extends('frontend.layouts.front_design')
@section('content')
    <div class="content-area" id="primary">
        <main class="site-main" id="main">
            <section id="post-724" class="post-724 page type-page status-publish hentry section section--singular">
                <div class="entry-content clearfix">
                    <h1>How can we <br />help you today?</h1>
                    <div class="wp-block-pixudio-spacer"><div class="hidden-lg-up" style="height:72px"></div><div class="hidden-md-down" style="height:72px"></div></div>
                    <div id="contact">
                        <div class="row double-gutters">
                            <div class="col-12 col-lg-3">
                                <h5 class="color--contact">Near dhobighat futsal</h5>
                                <p class="color--body-alt">{{$contact->street}}, <br>{{$contact->city}}, <br>Nepal</p>
                                <p class="text-body-2"><a style="color: white;">Tel +{{$contact->phone_1}} / {{$contact->phone_2}}</a><br><span>{{$contact->email}}</span></p>
                            </div>
                            <div class="col-12 col-lg-8 offset-lg-1 contactform">
                                <p class="big">
                                    Interested in working with us or just want to say hello?
                                </p>
                                <div role="form" class="wpcf7" id="wpcf7-f723-p724-o1" lang="en-US" dir="ltr">
                                    <div class="screen-reader-response"></div>
                                    <form id="contact_form" action="{{route('store.message')}}" method="post" class="wpcf7-form" enctype="multipart/form-data" onclick="" >@csrf
                                        <?php $test = false ?>
                                        @if(($errors->first('name')||$errors->first('subject')|| $errors->first('message')
                                             ||$errors->first('number')))
                                            <?php $test = true ?>
                                        @endif
                                        @if(Session::has('flash_message_error'))

                                                <div class="wp-block-ivory-alert alert pattern invert alert--primary" role="alert"><div class="alert__inner" ><strong>{!! session('flash_message_error') !!}</strong></div><button type="button" class="btn--transparent close" data-dismiss="alert" aria-label="Close"><span class="btn__copy"><i class="fa fa-times"></i></span></button></div>

                                        @endif
                                        @if(Session::has('flash_message_success'))

                                                <div class="wp-block-ivory-alert alert pattern invert alert--secondary" style="background-color: mediumspringgreen !important;" role="alert"><div class="alert__inner"><strong>{!! session('flash_message_success') !!}</strong></div><button type="button" class="btn--transparent close" data-dismiss="alert" aria-label="Close"><span class="btn__copy"><i class="fa fa-times"></i></span></button></div>

                                                {{--                                                        <button type="button" class="close" data-dismiss="alert"></button>--}}
{{--                                                <strong style="color: red;">{!! session('flash_message_success') !!}</strong>--}}
                                        @endif
{{--                                        <div style="display: none;">--}}
{{--                                            <input type="hidden" name="_wpcf7" value="723"/>--}}
{{--                                            <input type="hidden" name="_wpcf7_version" value="5.0.5"/>--}}
{{--                                            <input type="hidden" name="_wpcf7_locale" value="en_US"/>--}}
{{--                                            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f723-p724-o1"/>--}}
{{--                                            <input type="hidden" name="_wpcf7_container_post" value="724"/>--}}
{{--                                        </div>--}}
                                        <p><span class="wpcf7-form-control-wrap your-name contact"><input type="text"
                                                                                                  required
                                                                                                  name="name"
                                                                                                  value="" size="40"
                                                                                                  class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                                  aria-required="true"
                                                                                                  aria-invalid="false"
                                                                                                  placeholder="Name"/>
                                            <span class="help-block" style="color: #ff2320;">{{ $errors->first('name') }}</span></span>
                                        </p>
                                        <p><span class="wpcf7-form-control-wrap your-email contact"><input type="email"
                                                                                                   name="email"
                                                                                                   required
                                                                                                   value="" size="40"
                                                                                                   class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                   aria-required="true"
                                                                                                   aria-invalid="false"
                                                                                                   placeholder="Email"/>
                                            <span class="help-block" style="color: #ff2320;">{{ $errors->first('email') }}</span></span>
                                        </p>
                                        <p><span class="wpcf7-form-control-wrap your-email contact"><input type="text"
                                                                                                   name="number"
                                                                                                   required
                                                                                                   value="" size="40"
                                                                                                   class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                                   aria-required="true"
                                                                                                   aria-invalid="false"
                                                                                                   placeholder="Phone"/>
                                                          <span class="help-block" style="color: #ff2320;">{{ $errors->first('number') }}</span>
                                            </span>
                                        </p>
                                        <p><span class="wpcf7-form-control-wrap your-email contact">
                                            <select class="input1 select1" required name="subject" style="color: #8d929c;" >
                                                                    <option class="select" value="volunteer" style="color: #8d929c;">Volunteer</option>
                                                                     <option class="select" value="sponsor"  style="color: #8d929c;">Sponsor</option>
                                                                    <option class="select" value="sponsor"  style="color: #8d929c;">General Enquiry</option>
                                                                </select>
                                                <span class="help-block" style="color: #ff2320;">{{ $errors->first('subject') }}</span>
                                            </span>
                                        </p>

                                        <p><span class="wpcf7-form-control-wrap your-message contact"><textarea
                                                    name="message" cols="40" rows="10"
                                                    class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"
                                                    placeholder="How can we help?"></textarea>
                                            <span class="help-block" style="color: #ff2320;">{{ $errors->first('message') }}</span>
                                            </span>

                                        </p>
{{--                                        <p><input type="submit" value="Send message"/></p>--}}
{{--                                        <div class="wpcf7-response-output wpcf7-display-none"></div>--}}
                                       <p> <button  style="color: white;" id="contactSubmit" type="submit" class="btn btn--secondary site__call-to-action"><span style="color: white !important;">Send Message</span></button></p>
{{--                                            <span type="submit" class="btn btn--secondary site__call-to-action">Send Message</span>--}}

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
    @endsection
@section('styles')
    <style>
        .alert{
            background-color: red;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    @endsection
@section('scripts')
    <script>
        jQuery(document).ready(function ($) {
            $('#contact_form').submit(function (e) {
                e.preventDefault();

                var url = $(this).attr('action');

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: url,
                    method: "POST",
                    data: new FormData(this),
                    dataType: 'JSON',
                    contentType: false,
                    cache: false,
                    processData: false,
                    async: true,
                    beforeSend: function () {
                        $('#contactSubmit').text('');
                        $('#contactSubmit').append('<i style="color: white;" class="fa fa-refresh fa-spin"></i><span style="color: white;">&nbsp; Sending your message</span>')
                    },
                    success: function (resp) {
                        $('#contact_form').find("input[type=text], input[type=email], textarea").val("");
                        $('#contactSubmit').text('');
                        $('#contactSubmit').append(' <i class="fa fa-check"></i><span style="color: white;">&nbsp; Send message</span><i style="color: white !important;" class="icon-right-arrow">')
                    },
                    error: function (resp) {
                        console.log(resp);
                        $('#contactSubmit').text('');
                        $('#contactSubmit').append(' <i class="fa fa-times"></i><span style="color: white">&nbsp; Send message</span><i style="color: white !important;" class="icon-right-arrow">')
                    }
                })
            });
        });


    </script>
    @endsection
@section('styles')
    <style>
        @media only screen and (max-width: 575px) {
            /*.contact {*/
            /*   padding-right:9% !important;*/
            /*    padding-left:9% !important;*/
            /*}*/
            body. page--blocks{
                width: 90%;
            }
        }
    </style>

    @endsection
