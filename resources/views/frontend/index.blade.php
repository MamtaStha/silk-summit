@extends('frontend.layouts.front_design')
@section('content')
    <div class="site-content" id="content">
        <div class="content-area" id="primary">
            <main class="site-main" id="main">
                <section id="post-1470" class="post-1470 page type-page status-publish hentry">
                    <div class="entry-content clearfix">
                        <section class="wp-block-pixudio-pattern section section--first alignfull">
                            <div class="paper paper--150 paper--patterns in-view"
                                 data-elements="[{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-11.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:100,&quot;y&quot;:-300,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:100,&quot;y&quot;:-400,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-14.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-100,&quot;y&quot;:-350,&quot;scale&quot;:0.1},&quot;desktop&quot;:{&quot;x&quot;:-445,&quot;y&quot;:-455,&quot;scale&quot;:0.15}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-14.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:300,&quot;y&quot;:-100,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:400,&quot;y&quot;:50,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-13.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-105,&quot;y&quot;:-360,&quot;scale&quot;:0.2},&quot;desktop&quot;:{&quot;x&quot;:-450,&quot;y&quot;:-460,&quot;scale&quot;:0.3}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-08.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:255,&quot;y&quot;:-400,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:500,&quot;y&quot;:-500,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-13.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:95,&quot;y&quot;:-310,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:90,&quot;y&quot;:-410,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-08.png&quot;,&quot;rotate&quot;:190,&quot;mobile&quot;:{&quot;x&quot;:295,&quot;y&quot;:-100,&quot;scale&quot;:0.15},&quot;desktop&quot;:{&quot;x&quot;:390,&quot;y&quot;:30,&quot;scale&quot;:0.2}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-09.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-150,&quot;y&quot;:100,&quot;scale&quot;:0.1},&quot;desktop&quot;:{&quot;x&quot;:-450,&quot;y&quot;:150,&quot;scale&quot;:0.15}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-10.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-145,&quot;y&quot;:105,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:-445,&quot;y&quot;:155,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-11.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-220,&quot;y&quot;:-170,&quot;scale&quot;:0.2},&quot;desktop&quot;:{&quot;x&quot;:-600,&quot;y&quot;:-155,&quot;scale&quot;:0.25}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-14.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:120,&quot;y&quot;:220,&quot;scale&quot;:0.15},&quot;desktop&quot;:{&quot;x&quot;:100,&quot;y&quot;:250,&quot;scale&quot;:0.2}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-12.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-220,&quot;y&quot;:-170,&quot;scale&quot;:0.35},&quot;desktop&quot;:{&quot;x&quot;:-600,&quot;y&quot;:-155,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-09.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:-70,&quot;y&quot;:250,&quot;scale&quot;:0.2},&quot;desktop&quot;:{&quot;x&quot;:-300,&quot;y&quot;:320,&quot;scale&quot;:0.25}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-15.png&quot;,&quot;rotate&quot;:0,&quot;mobile&quot;:{&quot;x&quot;:120,&quot;y&quot;:220,&quot;scale&quot;:0.35},&quot;desktop&quot;:{&quot;x&quot;:100,&quot;y&quot;:250,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-07.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:30,&quot;mobile&quot;:{&quot;x&quot;:-80,&quot;y&quot;:140,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:-80,&quot;y&quot;:140,&quot;scale&quot;:0.3}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:90,&quot;mobile&quot;:{&quot;x&quot;:-250,&quot;y&quot;:-350,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:-250,&quot;y&quot;:-450,&quot;scale&quot;:0.3}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-07.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:90,&quot;mobile&quot;:{&quot;x&quot;:-220,&quot;y&quot;:-170,&quot;scale&quot;:0.4},&quot;desktop&quot;:{&quot;x&quot;:420,&quot;y&quot;:-170,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-07.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:230,&quot;mobile&quot;:{&quot;x&quot;:-200,&quot;y&quot;:-50,&quot;scale&quot;:0.4},&quot;desktop&quot;:{&quot;x&quot;:-380,&quot;y&quot;:180,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-07.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:180,&quot;mobile&quot;:{&quot;x&quot;:-50,&quot;y&quot;:-290,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:-520,&quot;y&quot;:-290,&quot;scale&quot;:0.3}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:180,&quot;mobile&quot;:{&quot;x&quot;:50,&quot;y&quot;:-200,&quot;scale&quot;:0.4},&quot;desktop&quot;:{&quot;x&quot;:300,&quot;y&quot;:-200,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:90,&quot;mobile&quot;:{&quot;x&quot;:100,&quot;y&quot;:-100,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:500,&quot;y&quot;:-100,&quot;scale&quot;:0.3}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:130,&quot;mobile&quot;:{&quot;x&quot;:-140,&quot;y&quot;:-330,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:-120,&quot;y&quot;:-350,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:-40,&quot;mobile&quot;:{&quot;x&quot;:180,&quot;y&quot;:-250,&quot;scale&quot;:0.4},&quot;desktop&quot;:{&quot;x&quot;:220,&quot;y&quot;:-450,&quot;scale&quot;:0.5}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:45,&quot;mobile&quot;:{&quot;x&quot;:-200,&quot;y&quot;:200,&quot;scale&quot;:0.4},&quot;desktop&quot;:{&quot;x&quot;:-200,&quot;y&quot;:200,&quot;scale&quot;:0.4}},{&quot;type&quot;:&quot;raster&quot;,&quot;src&quot;:&quot;http://mae-wordpress.pixudio.com/wp-content/uploads/2018/11/shape-06.png&quot;,&quot;blendMode&quot;:&quot;multiply&quot;,&quot;rotate&quot;:180,&quot;mobile&quot;:{&quot;x&quot;:150,&quot;y&quot;:50,&quot;scale&quot;:0.3},&quot;desktop&quot;:{&quot;x&quot;:150,&quot;y&quot;:50,&quot;scale&quot;:0.3}}]">
                                <canvas class="in-view__child in-view__child--fadein" data-paper-resize="true"
                                        data-paper-keepalive="true"></canvas>
                            </div>
                            <div class="container">
                                <div class="row in-view">
                                    <div class="col-12 col-xl-10 offset-xl-1"><h5
                                            class="in-view__child in-view__child--fadein"><span
                                                class="pattern__subtitle">{!! $header->sub_title !!}</span></h5>
                                        <h1 class="in-view__child in-view__child--fadein display-one"><span
                                                class="pattern__title">{!!$header->title  !!}</span></h1>
                                        <div
                                            class="row align-items-center margin-top in-view__child in-view__child--fadein justify-content-start">
                                            <div class="col-12 order-lg-1 col-lg-auto"><p><a
                                                        class="pattern__cta btn btn--custom btn--round"
                                                        style="background-color:#2f9dcd;color:#ffffff"
                                                        href="{{$header->register_form_link}}">{!! $header->button_text !!}</a></p></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

{{--                        speaker start--}}
                        <section class="wp-block-pixudio-speakers section">
                            <div class="speakers">
                                <div class="board board--right">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-md-6 order-md-2">
                                            <div class="board__image poster in-view">
                                                <figure><img class="speaker__image lazyload--el lazyload in-view__child"
                                                             data-id="1477"
                                                             data-src="{{asset('storage/'.$speaker1->image)}}"
                                                             alt="" width="445" height="600"/>
                                                    <div class="poster--cover in-view in-view__child"
                                                         data-offset="90%"></div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 order-md-1">
                                            <div class="board__copy in-view"><h2
                                                    class="speaker__name in-view__child in-view__child--fadein">
                                                   {!! $speaker1->name !!}</h2>
                                                <p class="speaker__title sub-head dashed in-view__child in-view__child--fadein">
                                                    {{$speaker1->position}}<br/> At <strong>{{$speaker1->company}}</strong><br/></p>
                                                <p class="speaker__desc indent in-view__child in-view__child--fadein">
                                                   {!! $speaker1->description !!}</p>
                                                <p class="board__copy--links indent in-view__child in-view__child--fadein">
{{--                                                    <span--}}
{{--                                                        class="sub-head dashed dashed--reverse dashed--hover in-view__child"><a--}}
{{--                                                            class="social-link--icon"--}}
{{--                                                            href="{{$speaker1->linkedin}}"--}}
{{--                                                            target="_blank"></a><a class="social-link--icon"--}}
{{--                                                                                   href="{{$speaker1->facebook}}"--}}
{{--                                                                                   target="_blank"></a><a--}}
{{--                                                            class="social-link--icon" href="{{$speaker1->twitter}}"--}}
{{--                                                            target="_blank"></a></span>--}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="board board--left">
                                    <div class="row align-items-end">
                                        <div class="col-12 col-md-6">
                                            <div class="board__image poster in-view">
                                                <figure><img class="speaker__image lazyload--el lazyload in-view__child"
                                                             data-id="1478"
                                                             data-src="{{asset('storage/'.$speaker2->image)}}"
                                                             alt="" width="445" height="600"/>
                                                    <div class="poster--cover in-view in-view__child"
                                                         data-offset="90%"></div>
                                                </figure>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="board__copy in-view"><h2
                                                    class="speaker__name in-view__child in-view__child--fadein">
                                                   {!! $speaker2->name !!}</h2>
                                                <p class="speaker__title sub-head dashed in-view__child in-view__child--fadein">
                                                    {{$speaker2->position}}<br/> At <strong>{{$speaker2->company}}</strong><br/></p>
                                                <p class="speaker__desc indent in-view__child in-view__child--fadein">
                                                   {!! $speaker2->description !!}</p>
                                                <p class="board__copy--links indent in-view__child in-view__child--fadein">
{{--                                                    <span--}}
{{--                                                        class="sub-head dashed dashed--reverse dashed--hover in-view__child"><a--}}
{{--                                                            class="social-link--icon"--}}
{{--                                                            href="http://mae-wordpress.pixudio.com/home-dark/www.linkedin.com"--}}
{{--                                                            target="_blank"></a><a class="social-link--icon"--}}
{{--                                                                                   href="http://mae-wordpress.pixudio.com/home-dark/www.facebook.com"--}}
{{--                                                                                   target="_blank"></a><a--}}
{{--                                                            class="social-link--icon" href="http://www.twitter.com"--}}
{{--                                                            target="_blank"></a></span>--}}
                                                </p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
{{--                        end of speaker--}}

{{--                        start of about card--}}
                        <div class="wp-block-pixudio-wildcard section">
                            <div class="reveal in-view reveal--primary reveal--left  reveal--fixed">
                                <div class="reveal__frame">
                                    <figure class="reveal__background lazyload--el lazyload" data-id="855"
                                            data-bg="http://mae-wordpress.pixudio.com/wp-content/uploads/2018/10/placeholder-stock-50.jpg"></figure>
                                </div>
                                <h3 class="reveal__title in-view__child in-view__child--fadein abt_text">{!! $about->title !!}</h3>
                                <div class="reveal__copy indent">
                                    <div class="row">
                                        <div class="col-12 col-md-2 in-view__child in-view__child--fadein "><h6
                                                class="reveal__action__label abt_text">Innovate</h6>
                                        </div>
                                        <div class="col-12 col-md-2 in-view__child in-view__child--fadein "><h6
                                                class="reveal__action__label abt_text">Enact</h6>
                                        </div>
                                        <div class="col-12 col-md-2 in-view__child in-view__child--fadein "><h6
                                                class="reveal__action__label abt_text">Inspire</h6>
                                        </div>
                                    </div>
                                    <p class="reveal__content in-view__child in-view__child--fadein abt_text">{!!$about->description  !!}</p>
                                </div>
                            </div>
                        </div>
{{--                        end of about card--}}


                        <div style="height:38px" aria-hidden="true" class="wp-block-spacer"></div>

{{--                        about workshop--}}
                        <div class="wp-block-pixudio-schedule schedule">
{{--                            <div class="wp-block-pixudio-schedule schedule reveal reveal--tertiary reveal--right">--}}
                            <div class="reveal in-view reveal--tertiary reveal--right  reveal--fixed " style="background-color: #2f7bbf">
                                <div class="reveal__frame">
                                    <figure class="reveal__background lazyload--el lazyload" data-id="855"
                                            data-bg="http://mae-wordpress.pixudio.com/wp-content/uploads/2018/10/placeholder-stock-50.jpg"></figure>
                                </div>
                                <h3 class="reveal__title in-view__child in-view__child--fadein ">About Workshops</h3>
                                <div class="reveal__copy indent">
                                    <div class="row">
                                        <div class="col-12 col-md-4 in-view__child in-view__child--fadein"><h6
                                                class="reveal__action__label">{!! $description->workshop_subtitle !!}</h6>
                                        </div>
                                    </div>
                                    <p class="reveal__content in-view__child in-view__child--fadein">{!!$description->workshop_description !!}</p>
                                </div>
                            </div>
                        </div>
{{--                        end of about workshop--}}
                        <div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>
                        <div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>


                        <div style="height:38px" aria-hidden="true" class="wp-block-spacer"></div>

{{--                        sponsor start--}}
                        <div class="wp-block-pixudio-wildcard section" style="background-color: grey !important; padding-top: 2%; padding-bottom: 2%;padding-left: 2%;padding-right: 2%; width: 110%">
                                <h1><span style="color: white; padding-left: 15%; padding-bottom: 5%" >{{$type->name}}</span> </h1>
                                <div class="reveal__copy container indent" style=" padding: 15px;">
                                    <div class="row">
                                        @foreach($type->sponsors as $sponsor)
                                            <div class="col-md-3">
                                                <img class="img-fluid img-thumbnail"
                                                     src="{{asset('storage/'.$sponsor->image)}}" alt="Smiley face"
                                                     style="padding-right: 15px; padding-top: 2%;padding-bottom: 2%"/>

                                            </div>
                                        @endforeach
                                    </div>

                                </div>

                        </div>


{{--                        <section class="wp-block-pixudio-sponsors section">--}}
{{--                            <div class="sponsors in-view">--}}
{{--                                <h3 class="in-view__child in-view__child--fadein">{{$type->name}}<br /></h3>--}}
{{--                                <div class="row padding-top margin-top">--}}
{{--                                    @foreach($type->sponsors as $sponsor)--}}
{{--                                    <div class="col-4 col-sm-2 in-view__child in-view__child--fadein">--}}
{{--                                        <div class="company">--}}
{{--                                            <figure><img class="lazyload--el lazyload" alt=""  style="width: 100%;" width="0" height="0" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{{asset('storage/'.$sponsor->image)}}" data-id="640" /></figure>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                        @endforeach--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </section>--}}


                        {{--                        end of sponsor--}}



                        {{--                        <div style="height:30px" aria-hidden="true" class="wp-block-spacer"></div>--}}




{{--                        start of countdown--}}
                        <section class="wp-block-pixudio-countdown section section--first alignfull">
                            <div class="paper paper--150 paper--stars in-view"
                                 data-elements="{&quot;count&quot;:50,&quot;radius&quot;:5}">
                                <canvas class="in-view__child in-view__child--fadein" data-paper-resize="true"
                                        data-paper-keepalive="true"></canvas>
                            </div>
                            <div class="container">
                                <div class="row in-view">
                                    <div class="col-12 col-xl-10 offset-xl-1 text-center"><h5
                                            class="in-view__child in-view__child--fadein">
                                            <span class="countdown__subtitle">Be a Part of our event</span></h5>
                                        <h1 class="margin-top in-view__child in-view__child--fadein"><span
                                                class="countdown__title">Silk Innovation Summit</span>
                                        </h1>
                                        <div class="margin-bottom in-view__child in-view__child--fadein">
                                            <div class="countdown row justify-content-center"
                                                 data-count="2019-10-09T16:03:00">
                                                <div class="countdown__el col-12 col-md-3" data-display="days">
                                                    <div class="countdown__count" id="days">00</div>
                                                    <div class="countdown__label">Days</div>
                                                </div>
                                                <div class="countdown__el col-12 col-md-3" data-display="hours">
                                                    <div class="countdown__count" id="hours">00</div>
                                                    <div class="countdown__label">Hours</div>
                                                </div>
                                                <div class="countdown__el col-12 col-md-3" data-display="minutes">
                                                    <div class="countdown__count" id="minutes">00</div>
                                                    <div class="countdown__label">Minutes</div>
                                                </div>
                                                <div class="countdown__el col-12 col-md-3" data-display="seconds">
                                                    <div class="countdown__count" id="seconds">00</div>
                                                    <div class="countdown__label">Seconds</div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="in-view__child in-view__child--fadein"><a
                                                class="countdown__cta btn btn--secondary"
                                                href="{{$header->register_form_link}}" style="color: white;">Register Now</a></p>
                                        <p class="in-view__child in-view__child--fadein"><a
                                                class="sub-head-2 countdown__add-to-calendar"
                                                target="_blank"
                                                href="https://calendar.google.com/event?action=TEMPLATE&tmeid=NjA2bTczZHB2NWF0cTQwajJmYWF0aWxwdGQgaGVsbG9AcGl4dWRpby5jb20&tmsrc=hello@pixudio.com"><i
                                                    class="fa fa-plus-circle"></i>   <span
                                                    class="countdown__add-to-calendar__label">Add to calendar</span></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </section>
{{--                        end of countdown--}}
                    </div>
                </section>
            </main>
        </div>
    </div>
@endsection
@section('styles')
    <style>
        .red{
            background-color: red;
        }
        .text{
            color: black !important;
        }
        .abt_text{
            color: white !important;
        }
        @media (max-width: 770px) {

        }

    </style>
    @endsection
@section('scripts')
    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("March, 2020 10:00:00").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Output the result in an element with id="demo"
            document.getElementById("days").innerHTML = days ;
            document.getElementById("hours").innerHTML = hours ;
            document.getElementById("minutes").innerHTML = minutes ;
            document.getElementById("seconds").innerHTML = seconds ;

            // If the count down is over, write some text
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>

@endsection

