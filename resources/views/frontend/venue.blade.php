@extends('frontend.layouts.front_design')
@section('content')
    <div class="content-area" id="primary">
        <main class="site-main" id="main" style="margin-top: 120px;">
            <section id="post-786" class="post-786 page type-page status-publish hentry">
                <div class="contact-map" id="map-canvas" style="width: 100%;"> </div>
                <div class="entry-content clearfix">
                    <section class="wp-block-mae-map venue section section--fit">
                        <div class="row">
                            <div class="col-12 col-lg-6 in-view"><h1
                                    class="map__title in-view__child in-view__child--fadein">{{$venue->title}}</h1>
                                <p class="map__subtitle sub-head dashed in-view__child in-view__child--fadein">{{$venue->room}} {{$venue->building}}<br/>{{$venue->street}}, {{$venue->city}}</p>
                                <p class="map__desc indent in-view__child in-view__child--fadein">{!! $venue->description !!}</p>
                                </div>
                        </div>
                    </section>
                </div>
            </section>
        </main>
    </div>
    @endsection
@section('styles')
    <style>
        #map-canvas{
            width:1500px;
            height: 550px;
        }

    </style>
@endsection
@section('scripts')
    {{--    <script--}}
    {{--        src="https://code.jquery.com/jquery-3.1.1.js"--}}
    {{--        integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="--}}
    {{--        crossorigin="anonymous"></script>--}}
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?language=en&key=AIzaSyAd1iF0qOMcbvehRChx4F1PxaB2MZPsUHc">
    </script>

    <script>
        $(document).ready(function () {
            var lat = 27.676234;
            var lng = 85.308495;

            if (!lat || !lng)
            {
                lat = 27.72;
                lng = 85.36;
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'),{
                center:{
                    lat: lat,
                    lng: lng
                },
                zoom:15
            });

            var marker = new google.maps.Marker({
                position:{
                    lat: lat,
                    lng: lng
                },
                map:map
            });

            // //open in google maps
            // // var gotoMapButton = document.createElement("div");
            // // gotoMapButton.setAttribute("style", "margin: 5px; border: 1px solid; padding: 1px 12px; font: bold 11px Roboto, Arial, sans-serif; color: #000000; background-color: #FFFFFF; cursor: pointer;");
            // // gotoMapButton.innerHTML = "Open in Google Maps";
            // // map.controls[google.maps.ControlPosition.TOP_RIGHT].push(gotoMapButton);
            // // google.maps.event.addDomListener(gotoMapButton, "click", function () {
            // //     var url = 'https://www.google.com/maps?q=' + encodeURIComponent(marker.getPosition().toUrlValue());
            // //     // you can also hard code the URL
            // //     window.open(url);
            // });
        });
    </script>
@endsection
