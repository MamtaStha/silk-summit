<footer class="site-footer" id="colophon" role="contentinfo">
    <div class="widget-area" role="complementary">
        <div class="container">
            <div class="row in-view">
                <div class="col-12 col-xl-10 offset-xl-1">
                    <div class="row widget-area__column-4">
                        <div class="col-12 col-lg-auto">
                            <div class="widget">
                                <div class="widget_socials">
                                    <div class="site-branding" itemscope itemtype="http://schema.org/Organization">
                                        <a href="{{route('index')}}" class="custom-logo-link" rel="home" itemprop="url"><img src="{{asset('storage/'.$header->logo)}}" class="custom-logo" alt="Mae" itemprop="logo" /></a></div>
                                    <nav class="widget_socials__list" aria-label="Social Menu" role="navigation">
                                        <div class="menu-social-links-menu-container"><ul id="social-menu" class="menu"><li id="menu-item-20" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-20" style="display: none;"><a href="{{$contact->twitter}}" class="widget_socials__item social-link--icon"><span class="sr-only">Twitter</span></a></li>
                                                <li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22"><a href="{{$contact->facebook}}" class="widget_socials__item social-link--icon"><span class="sr-only">Facebook</span></a></li>
                                                <li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-22" style="display: none;"><a href="{{$contact->instagram}}" class="widget_socials__item social-link--icon"><span class="sr-only">Instagram</span></a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-auto"><section id="nav_menu-7" class="widget widget_nav_menu"><h2 class="widget-title">Past events</h2><div class="menu-past-events-container"><ul id="menu-past-events" class="menu"><li id="menu-item-403" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-403"><a href="#">New York</a></li>
                                        <li id="menu-item-405" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-405"><a href="#">Paris</a></li>
                                        <li id="menu-item-404" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-404"><a href="#">Amsterdam</a></li>
                                        <li id="menu-item-402" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-402"><a href="#">London</a></li>
                                    </ul></div></section></div><div class="col-12 col-lg-auto"><section id="nav_menu-9" class="widget widget_nav_menu"><h2 class="widget-title">Legal terms</h2><div class="menu-legal-terms-container"><ul id="menu-legal-terms" class="menu"><li id="menu-item-411" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-411"><a href="#">Privacy Policy</a></li>
                                        <li id="menu-item-412" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-412"><a href="#">Code of Conduct</a></li>
                                    </ul></div></section></div><div class="col-12 col-lg-auto"><section id="nav_menu-5" class="widget widget_nav_menu">
                                <h2 class="widget-title">Contact</h2>
                                <div class="menu-contact-container">
                                    <ul id="menu-contact" class="menu">
                                        <li id="menu-item-185" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185">
                                                <span>{{$contact->email}}</span>

                                        </li>
                                        <li id="menu-item-408" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-408"> <a href="{{route('contact')}}" ></a> Be a sponsor</li>
                                    </ul></div></section></div> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-info">
        <div class="container">
            <div class="row in-view">
                <div class="col-12 col-xl-10 offset-xl-1">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="site-info__col-left">
                                <a class="sub-head" href="https://themeforest.net/user/pixudio/portfolio">Copyright © 2019 </i>Silk Innovation. All rights reserved.</a> </div>
                        </div>
                        <div class="col-12 col-md-6 text-md-right">
                            <div class="site-info__col-right">
                                <a class="sub-head dashed dashed--reverse in-view__child in-view__child--in" href="https://silkinnovation.com.np/">Powered by <b>Silk Innovation</b></a> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="go-up" id="up">
    <a class="go-up__btn" href="#movetotop">
        <span class="btn btn--primary btn--circle"><i style="color: white" class="fa fa-chevron-up"></i></span>
    </a>
</div>
