<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
    <link rel="dns-prefetch" href="http://fonts.googleapis.com">
    <link rel="dns-prefetch" href="http://s.w.org">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <script type="2ffb15254de5313056f71216-text/javascript">
        (function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);
    </script>
    <title>Home — Dark &#8211; Mae</title>
    <link rel='dns-prefetch' href='http://fonts.googleapis.com'/>
    <link rel='dns-prefetch' href='http://s.w.org'/>
    <link href='https://fonts.gstatic.com' crossorigin rel='preconnect'/>
    <link rel="alternate" type="application/rss+xml" title="Mae &raquo; Feed" href="../feed/index.html"/>
    <link rel="alternate" type="application/rss+xml" title="Mae &raquo; Comments Feed"
          href="../comments/feed/index.html"/>
    <script type="2ffb15254de5313056f71216-text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/mae-wordpress.pixudio.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);

    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
        <style type="text/css">
            .side-menu--more{
                display: none !important;
            }
        </style>
    <link rel='stylesheet' id='contact-form-7-css'
          href='{{asset('frontend/wp-content/plugins/contact-form-7/includes/css/styles-ver=5.0.5.css')}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='wp-block-library-css'
          href='{{asset('frontend/wp-content/plugins/gutenberg/build/block-library/style-ver=1543492841.css')}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='pixudio-font-awesome-css'
          href='{{asset('frontend/wp-content/themes/mae/assets/css/fontawesome-all.min-ver=4.9.8.css')}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='pixudio-fonts-css'
          href='https://fonts.googleapis.com/css?family=Karla:700,700italic,italic,regular%7CPoppins:100,100italic,200,200italic,300,300italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic,italic,regular&subset=latin,latin-ext'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='pixudio-style-css' href='{{asset('frontend/wp-content/themes/mae/style-ver=4.9.8.css')}}'
          type='text/css' media='all'/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
    <script type="2ffb15254de5313056f71216-text/javascript"
            src='{{asset('frontend/wp-includes/js/jquery/jquery-migrate.min-ver=1.4.1.js')}}'></script>
    <link rel='https://api.w.org/' href='{{asset('frontend//wp-json/index.html')}}'/>
    <style type="text/css" id="custom-theme-colors">

        /**
         * Mae color palette
         */

        /* Main
        ========================================================================== */
        body {
            background-color: #434851;
        }

        /* Layouts
        ========================================================================== */
        .dashed:before,
        .dashed:after {
            background-color: #fff;
        }

        .dashed--hover,
        a.dashed {
            cursor: default;
        }

        .dashed--hover:not(.dashed--vertical):hover:after,
        a.dashed:not(.dashed--vertical):hover:after {
            background-color: rgba(255, 255, 255, 0.1);
        }

        .board__copy--links a {
            color: #fff;
        }

        .board__copy--links a:hover {
            color: rgba(255, 255, 255, 0.65);
        }

        .reveal--darken,
        .reveal--darken p,
        .reveal--darken a {
            color: #434851;
        }

        .reveal--darken:before {
            background-color: #fff;
        }

        .reveal--darken .sub-head,
        .reveal--darken a.sub-head {
            color: inherit !important;
        }

        .reveal--darken .dashed:before,
        .reveal--darken .dashed:after {
            background-color: #434851;
        }

        .reveal--darken .dashed.dashed--hover:not(.dashed--vertical):hover:after,
        .reveal--darken a.dashed:not(.dashed--vertical):hover:after {
            background-color: rgba(67, 72, 81, 0.3);
        }

        .clipcard:last-child .clipcard__body:after {
            border-color: transparent;
        }

        .clipcard__link:after {
            background-color: #fff;
        }

        .clipcard__subtitle span {
            color: inherit;
        }

        /* Page
        ========================================================================== */
        .single-content__footer a:not(:hover) {
            color: inherit;
        }

        /* Masonry
        ========================================================================== */
        .indexed-list__wrap {
            background-color: #434851;
            -webkit-box-shadow: 0px 3px 24px 0px rgba(255, 255, 255, 0.25);
            box-shadow: 0px 3px 24px 0px rgba(255, 255, 255, 0.25);
        }

        .indexed-list__wrap__copy {
            background-color: #434851;
        }

        .indexed-list__wrap .sub-head-2 * {
            color: inherit;
        }

        .indexed-list__wrap__title a:not(:hover) {
            color: inherit;
        }

        .indexed-list__wrap__desc {
            color: #d4d7de;
        }

        .indexed-list__wrap__link a:not(:hover) {
            color: inherit;
        }

        .indexed-list__box {
            -webkit-box-shadow: 0px 3px 24px 0px rgba(255, 255, 255, 0.25);
            box-shadow: 0px 3px 24px 0px rgba(255, 255, 255, 0.25);
        }

        .indexed-list__box__cell {
            color: #434851;
        }

        .indexed-list__box__badge {
            color: #fff;
        }

        .indexed-list__box .sub-head-2 {
            color: inherit;
        }

        .indexed-list__box .sub-head-2 * {
            color: inherit;
        }

        .indexed-list__box__title a:not(:hover) {
            color: inherit;
        }

        /* Related
        ========================================================================== */
        .related__box {
            -webkit-box-shadow: 0px 3px 24px 0px rgba(255, 255, 255, 0.25);
            box-shadow: 0px 3px 24px 0px rgba(255, 255, 255, 0.25);
        }

        .related__box__table:before {
            background-color: #fff;
        }

        .related__box__cell {
            color: #434851;
        }

        /* CTA
        ========================================================================== */
        .cta--darken {
            color: #434851;
        }

        .cta--darken:before {
            background-color: #fff;
        }

        .cta--darken .dashed:before,
        .cta--darken .dashed:after {
            background-color: #434851;
        }

        .cta--darken .dashed.dashed--hover:not(.dashed--vertical):hover:after,
        .cta--darken a.dashed:not(.dashed--vertical):hover:after {
            background-color: rgba(67, 72, 81, 0.3);
        }

        /* Carousel
        ========================================================================== */
        .carousel__dots {
            color: #fff;
        }

        .carousel__dots--light {
            color: #434851;
        }

        /* Mediabox
        ========================================================================== */
        .mediabox-wrap {
            background-color: rgba(67, 72, 81, 0.8);
        }

        /* Tabs
        ========================================================================== */
        .tabs__nav,
        .tabs__nav:hover,
        .tabs__nav:focus {
            color: inherit;
        }

        .reveal--primary .tabs__nav:after {
            border-bottom-color: inherit;
        }

        .reveal--sponsor{
            background-color: gray !important;
        }
        /* Header
        ========================================================================== */
        .has-header-background .site-header__wrap {
            background-color: rgba(67, 72, 81, 0.98);
        }

        .headroom--not-top:not(.headroom--unpinned) .site-header__wrap {
            background-color: rgba(67, 72, 81, 0.98);
            -webkit-box-shadow: 0 3px 16px #3e434b;
            box-shadow: 0 3px 16px #3e434b;
        }

        .site-header__mobile button {
            color: #abb2bf;
            background-color: transparent;
        }

        .site-header__nav ul.menu > li > a {
            color: inherit;
        }

        .site-header__nav ul.menu > li:not(.menu-item-has-children) > a:after {
            background-color: #fff;
        }

        .site-header__nav ul.menu ul {
            background-color: #484d57;
            -webkit-box-shadow: 0 6px 22px #3e434b;
            box-shadow: 0 6px 22px #3e434b;
        }

        /* Side navigation
        ========================================================================== */
        .site-sidenav__overlay {
            background-color: rgba(67, 72, 81, 0.8);
        }

        .site-sidenav__elements {
            background-color: #434851;
            -webkit-box-shadow: 8px 0 24px #3e434b;
            box-shadow: 8px 0 24px #3e434b;
        }

        .site-sidenav__branding {
            border-bottom: solid 1px rgba(255, 255, 255, 0.05);
        }

        .site-sidenav__nav a,
        .site-sidenav__nav a:hover {
            color: inherit;
        }

        .site-sidenav__nav ul.sub-menu a {
            color: #d4d7de;
        }

        .site-sidenav__cat {
            border-top: solid 1px rgba(255, 255, 255, 0.05);
        }

        /* Footer
        ========================================================================== */
        .site-footer .widget-area a:not(:hover) {
            color: inherit;
        }

        .site-info {
            background-color: #434851;
        }

        /* Typography
        ========================================================================== */
        body,
        button,
        input,
        select,
        textarea {
            color: #fff;
        }

        .btn a,
        .sub-head a {
            color: inherit;
        }

        .sub-head,
        a.sub-head {
            color: #fff;
        }

        .text-body-3 {
            color: #d4d7de;
        }

        .sub-head-2 {
            color: #d4d7de;
        }

        .sub-head-2 a:not(:hover) {
            color: inherit;
        }

        blockquote {
            color: #fff;
        }

        pre {
            color: #fff;
        }

        code,
        kbd,
        tt,
        var {
            color: #abb2bf;
            background-color: #434851;
        }

        mark {
            color: #434851;
        }

        ins {
            color: #fff;
        }

        /* Forms
        ========================================================================== */
        label {
            color: #fff;
        }

        div.mce_inline_error {
            color: inherit !important;
            background-color: transparent !important;
        }

        /* Buttons
        ========================================================================== */
        .btn {
            -webkit-box-shadow: 0 11px 22px rgba(67, 72, 81, 0.2);
            box-shadow: 0 11px 22px rgba(67, 72, 81, 0.2);
        }

        .btn,
        .btn:hover,
        .btn:focus,
        .btn:active {
            color: #fff;
        }

        .btn:hover {
            -webkit-box-shadow: 0 6px 14px rgba(67, 72, 81, 0.25);
            box-shadow: 0 6px 14px rgba(67, 72, 81, 0.25);
        }

        .btn:active {
            -webkit-box-shadow: 0 2px 6px rgba(67, 72, 81, 0.5);
            box-shadow: 0 2px 6px rgba(67, 72, 81, 0.5);
        }

        .btn--primary,
        .btn--primary:hover,
        .btn--primary:focus,
        .btn--primary:active {
            color: #434851;
        }

        .btn--secondary,
        .btn--secondary:hover,
        .btn--secondary:focus,
        .btn--secondary:active {
            color: #434851;
        }

        /* Pricing
        ========================================================================== */
        .pricing__item {
            border: solid 1px #565a63;
        }

        /* Alert
        ========================================================================== */
        .alert {
            background-color: #fff;
            color: #434851;
        }

        .alert--secondary {
            color: #fff;
        }

        /* Progress Bsr
        ========================================================================== */
        .progress__bar {
            background-color: #fff;
        }

        /* Accoirdion
        ========================================================================== */
        .accordion__header label svg {
            fill: #fff;
        }

        /* Customizer
        ========================================================================== */
        .color--body {
            color: #fff;
        }

        .background-color--body {
            background-color: #fff;
        }

        .color--body-alt {
            color: #d4d7de;
        }

        .background-color--body-alt {
            background-color: #d4d7de;
        }

        .color--body-lighten {
            color: #434851;
        }

        .background-color--body-lighten {
            background-color: #434851;
        }

        .color--code {
            color: #abb2bf;
        }

        .background-color--code {
            background-color: #abb2bf;
        }

        .color--code-background {
            color: #434851;
        }

        .background-color--code-background {
            background-color: #434851;
        }


        a,
        .color--primary,
        .color--link,
        .site-footer .widget-area .widget_calendar tbody a {
            color: #89158F;
        }


        a:hover,
        a:focus,
        .color--secondary,
        .color--link-hover,
        .comments-area .required,
        .site-footer .widget-area .widget_calendar tbody a:hover,
        .site-footer .widget-area .widget_calendar tbody a:focus {
            color: #89158F;
        }

        .color--tertiary {
            color: #89158F;
        }

        button,
        button:hover,
        button:focus,
        button:active,
        html [type="button"],
        html [type="button"]:hover,
        html [type="button"]:focus,
        html [type="button"]:active,
        [type="reset"],
        [type="reset"]:hover,
        [type="reset"]:focus,
        [type="reset"]:active,
        .poster--cover:after,
        .reveal--tertiary .reveal__frame,
        .cta--tertiary:before,
        .single-content__tags a,
        .indexed-list__box,
        .indexed-list__box__badge,
        .related__box,
        .site-footer .widget-area,
        pre,
        mark,
        ins,
        .btn,
        .btn:hover,
        .btn:focus,
        .btn:active,
        .background-color--tertiary,
        .embed-responsive__cover:after,
        .links__item__anchor {
            background-color: #89158F;
        }

        .widget_calendar table,
        .widget_calendar table tr,
        .widget_calendar table td,
        .widget_calendar table th,
        .widget_calendar tfoot,
        .widget_calendar caption {
            border-color: #89158F;
        }

        .not-found__icon svg {
            fill: #89158F;
        }

        [type="submit"],
        [type="submit"]:hover,
        [type="submit"]:focus,
        [type="submit"]:active,
        .page-template-page-template-coming-soon,
        .reveal--primary .reveal__frame,
        .cta--primary:before,
        .btn--primary,
        .btn--primary:hover,
        .btn--primary:focus,
        .btn--primary:active,
        .background-color--primary,
        .background-color--link,
        .sticky__tag {
            background-color: #89158F;
        }

        .reveal--secondary .reveal__frame,
        .cta--secondary:before,
        .btn--secondary,
        .btn--secondary:hover,
        .btn--secondary:focus,
        .btn--secondary:active,
        .background-color--secondary,
        .background-color--link-hover,
        .nav-links .current {
            background-color: #89158F;
        }

        .tabs__nav:after,
        blockquote,
        .wp-block-quote:not(.is-large),
        .site-header__nav ul.menu ul a:before {
            border-color: #89158F;
        }

        .site-header__nav ul.menu > li.menu-item-has-children:after {
            border-color: #89158F;
        }

        .monkey svg path:nth-child(2),
        .monkey svg path:nth-child(3) {
            fill: #89158F;
        }
    </style>
    <style type="text/css" id="custom-theme-fonts">

        /**
         * Mae fonts
         */


        body,
        button,
        input,
        select,
        textarea {
            font-family: "Karla", sane-serif;
            font-weight: 400;
        }



        button,
        html [type="button"],
        [type="reset"],
        [type="submit"],
        .btn,
        .sub-head,
        .text-body-2,
        blockquote,
        .indexed-list__wrap__link a,
        .site-header__nav ul > li > a,
        .site-sidenav__nav a,
        .comment-form label,
        .nav-links,
        .links__item,
        .page-links,
        .sticky__tag,
        .pingback,
        .widget_calendar caption,
        .widget_calendar tfoot td,
        table th,
        .related__box__title {
            font-family: "Karla", sane-serif;
            font-weight: 700;
        }
        s

        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        .display-one,
        .tabs__nav {
            font-family: "Poppins", sane-serif;
            font-weight: 700;
        }
    </style>
    @yield('styles')
    <link rel="icon" href="{{asset('frontend/wp-content/uploads/2018/06/favicon.png')}}" sizes="32x32"/>
    <link rel="icon" href="{{asset('frontend/wp-content/uploads/2018/06/favicon.png')}}" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed" href="{{asset('frontend/wp-content/uploads/2018/06/favicon.png')}}"/>
    <meta name="msapplication-TileImage" content="{{asset('frontend/wp-content/uploads/2018/06/favicon.png')}}"/>

</head>
<body class="page-template-default page page-id-1470 wp-custom-logo page--blocks">
<div class="site" id="movetotop">

    <!-- start of Header !-->
@include('frontend.layouts.front_header')
<!-- end of Header demo five html !-->

    @yield('content')


    @include('frontend.layouts.front_footer')

    <div class="site-sidenav" id="sidenav">
        <div class="site-sidenav__elements side-menu-swipeable">
            <div class="site-sidenav__branding">
                <div class="site-branding" itemscope itemtype="http://schema.org/Organization">
                    <a href="{{route('index')}}" class="custom-logo-link" rel="home" itemprop="url"><img
                            src="{{asset('storage/'.$header->logo)}}" class="custom-logo" alt="Silk Innovation"
                            itemprop="logo"/></a></div>
            </div>
            <div class="site-sidenav__nav">
                <div class="site-sidenav__menu">
                    <nav class="sidenav-navigation" aria-label="Site Mobile Menu" role="navigation">
                        <div class="menu-sidenav-menu-container">
                            <div class="menu-top-menu-container">
                                <ul id="sidenav-menu" class="menu">


                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-686">
                                        <a href="{{route('workshop')}}">Workshop</a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-686">
                                        <a href="{{route('schedule')}}">Schedule</a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-686">
                                        <a href="{{route('sponsor')}}">Sponsor</a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-686">
                                        <a href="{{route('venue')}}">Venue</a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-686">
                                        <a href="{{route('contact')}}">Contact</a>
                                    </li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-686">
                                        <a href="{{route('comming_soon')}}">Comming Soon</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="site-sidenav__cat hidden-md-up"><a class="btn btn--secondary site__call-to-action" style="color: white;"
                                                           href="https://docs.google.com/forms/d/e/1FAIpQLSdmut6jMJr5nHoFKEXCuSBg-H1REVF8vZSuOI-gKQjCEXan7A/viewform">Register
                    now</a></div>
        </div>
        <button class="site-sidenav__mobile--trigger">
            <span class="site-header__mobile__icon"></span>
        </button>
    </div>
</div>
<script data-cfasync="false"
        src="{{asset('frontend/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
<script type="2ffb15254de5313056f71216-text/javascript">
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/mae-wordpress.pixudio.com\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}}};
/* ]]> */

</script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/plugins/contact-form-7/includes/js/scripts-ver=5.0.5.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/classie.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/noframework.waypoints.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/paper-core.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/paper-animate-browser.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/headroom.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/instafeed.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/hammer.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/masonry.pkgd.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/mediabox.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/is-touch-device.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/resize-sensor.min-ver=1529656978.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/animated-scroll-to.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/siema.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/lazysizes.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/lib/ls.unveilhooks.min-ver=1529656599.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript">
/* <![CDATA[ */
var pixudio = {"colors":{"primary":"#89158F","secondary":"#89158F","tertiary":"#89158F","custom":false,"palette":"#89158F-#89158F-#89158F"},"animationOnTouchScreens":"activate","templateURL":"http:\/\/mae-wordpress.pixudio.com\/wp-content\/themes\/mae\/"};
/* ]]> */

</script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-content/themes/mae/assets/js/app.min-ver=1541600633.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript"
        src='{{asset('frontend/wp-includes/js/wp-embed.min-ver=4.9.8.js')}}'></script>
<script type="2ffb15254de5313056f71216-text/javascript">
    (function ($) {
        var isVideoLink = function (url) {
            return !!(
                url.match(/mailto:([^\?]*)/) ||
                url.match(/^(https?\:\/\/)?((?:www\.)?youtube\.com|youtu\.?be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/) ||
                url.match(/^(https?\:\/\/)?(?:www\.)?vimeo.com\/(?:channels\/|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/)
            );
        };
        var a = new RegExp('/' + window.location.host + '/');
        var tutorials = new RegExp('PLpv2l-pEpX1DsQCrFtBQg15GWXbE4h8mV');
        setTimeout(function () {
            $('a:not([href="#"]):not(.video-popup)').each(function () {
                if (!a.test(this.href)) {
                    if (false === isVideoLink(this.href) || tutorials.test(this.href)) {
                        $(this).click(function (event) {
                            event.preventDefault();
                            event.stopPropagation();
                            window.open(this.href, '_blank');
                        });
                    }
                }
            });
        }, 300);
    })(jQuery);

</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-64346230-6"
        type="2ffb15254de5313056f71216-text/javascript"></script>
<script type="2ffb15254de5313056f71216-text/javascript">
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-64346230-6');

</script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js"
        data-cf-settings="2ffb15254de5313056f71216-|49" defer=""></script>
@yield('scripts')
</body>
</html>
