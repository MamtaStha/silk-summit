
<header class="site-header" id="masthead">
    <div class="site-header__wrap">
        <div class="container">
            <div class="row in-view">
                <div class="col-12 col-xl-10 offset-xl-1">
                    <div class="site-header__elements">
                        <div class="site-header__mobile hidden-lg-up">
                            <button class="site-header__mobile--trigger side-menu-trigger">
                                <span class="site-header__mobile__icon"></span>
                            </button>
                        </div>
                        <div class="site-header__branding">
                            <div class="site-branding" itemscope itemtype="http://schema.org/Organization">
                                <a href="{{route('index')}}" class="custom-logo-link" rel="home" itemprop="url"><img src="{{asset('storage/'.$header->logo)}}" class="custom-logo" alt="Silk Innovation" itemprop="logo" /></a></div>
                        </div>
                        <div class="site-header__nav hidden-md-down">
                            <div class="site-header__menu">
                                <nav class="main-navigation" aria-label="Top Menu" role="navigation">
                                    <div class="menu-top-menu-container">
                                        <div class="menu-top-menu-container">
                                            <ul id="top-menu" class="menu">
{{--                                                <li id="menu-item-795" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-795"><a href="{{route('speaker')}}">Speaker</a>--}}
{{--                                                </li>--}}
{{--                                                <li id="menu-item-798" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-795"><a href="{{route('about')}}">About</a>--}}
{{--                                                </li>--}}
                                                <li id="menu-item-793" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-793"><a href="{{route('workshop')}}">Workshops</a>
                                                </li>
                                                <li id="menu-item-793" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-794"><a href="{{route('schedule')}}">Schedule</a>
                                                </li>
                                                <li id="menu-item-796" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-795"><a href="{{route('venue')}}">Venue</a>
                                                </li>
                                                <li id="menu-item-792" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-796"><a href="{{route('sponsor')}}">Sponsor</a>
                                                </li>
                                                <li id="menu-item-799" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-797"><a href="{{route('contact')}}">Contact</a>
                                                </li>
                                                <li id="menu-item-799" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-798"><a href="{{route('comming_soon')}}">Comming&nbsp;Soon</a>
                                                </li>




                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="site-header__cta hidden-sm-down button-register">
                            <a class="btn btn--secondary site__call-to-action button-register" href="{{$header->register_form_link}}" style="background-color: #89158F !important;"> <span style="color: white !important;">Register now</span> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@section('styles')
    <style>
        .button-register{
            background-color: #89158F;
        }
        .color--contact{
            background-color: #35033d !important;
        }
    </style>

    @endsection

