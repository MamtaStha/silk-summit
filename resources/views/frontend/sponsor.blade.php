@extends('frontend.layouts.front_design')
@section('content')
    <div class="content-area" id="primary">
        <main class="site-main" id="main">
            <section id="post-780" class="post-780 page type-page status-publish hentry section section--singular">
                <header class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-10 offset-xl-1">
                                <h1 class="page-title">Sponsors</h1> <h5 class="page-subtitle">{!! $description->sponsor_title !!}</h5> </div>
                        </div>
                    </div>
                </header>
                <div class="entry-content clearfix">
                    <div class="wp-block-pixudio-columns columns-block in-view">
                        <div class="columns columns-1 columns-md-2 columns-lg-2">
                            <div class="wp-block-pixudio-column in-view__child in-view__child--fadein">
                                <p class="dashed in-view__child">{!! $description->sponsor_description !!}</p>
                            </div>
                            <div class="wp-block-pixudio-column in-view__child in-view__child--fadein">
                                <p></p>
                            </div>
                        </div>
                    </div>
                    <div class="wp-block-pixudio-spacer">
                        <div class="hidden-lg-up" style="height:60px"></div>
                        <div class="hidden-md-down" style="height:60px"></div>
                    </div>
                    @foreach($sponsor_type as $type)
                    <section class="wp-block-pixudio-sponsors section">
                        <div class="sponsors in-view"><h3 class="in-view__child in-view__child--fadein">{{$type->name}}</h3>
                            <div class="row padding-top margin-top">
                                @foreach($type->sponsors as $sponsor)
                                <div class="col-4 col-sm-2 in-view__child in-view__child--fadein">
                                    <div class="company">
                                        <figure class=""><img class="lazyload--el lazyload" alt="" style="width: 100%" height="0"
                                                              src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                              data-src="{{asset('storage/'.$sponsor->image)}}"
                                                              data-id="703"/></figure>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                        </div>
                    </section>

@endforeach





















{{--                    media Partner--}}
{{--                    @foreach($sponsor_type as $type)--}}

{{--                    <div class="wp-block-pixudio-schedule schedule "@if($loop->index %2==0)style="margin-left: -10px;" @else style="margin-right: -10px;"@endif">--}}
{{--                        <div @if ($loop->index %2==0)class="reveal in-view  reveal--fixed reveal--sponsor reveal--right right " @else class="reveal in-view  reveal--fixed reveal--sponsor  reveal--left  left" @endif >--}}
{{--                            <div class="reveal__frame red"></div>--}}
{{--                            <h3 class="reveal__title in-view__child in-view__child--fadein " style="color: white;"><span> <p style="margin-left:90px;">{{$type->name}}</p></span></h3>--}}
{{--                            <div class="reveal__copy indent">--}}
{{--                                <div class="row padding-top margin-top">--}}
{{--                                    @foreach($type->sponsors as $sponsor)--}}
{{--                                        <div class="col-4 col-sm-2 in-view__child in-view__child--fadein">--}}
{{--                                            <div class="company">--}}
{{--                                                <figure class=""><img class="lazyload--el lazyload" alt="" width="30%" height="60%"--}}
{{--                                                                      src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="--}}
{{--                                                                      data-src="{{asset('storage/'.$sponsor->image)}}"--}}
{{--                                                                      data-id="707"/></figure>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    @endforeach--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                        <div style="height:48px" aria-hidden="true" class="wp-block-spacer"></div>--}}
{{--                    @endforeach--}}
{{--  end of media partner--}}




















{{--        speaker --}}
{{--                @foreach($sponsor_type as $type)--}}
{{--                <div class="content-area" id="primary">--}}
{{--                    <main class="site-main" id="main">--}}
{{--                        <section id="post-778" class="post-778 page type-page status-publish hentry section section--singular">--}}
{{--                            <header class="page-header">--}}
{{--                                <div class="container">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-12 col-xl-10 offset-xl-1">--}}
{{--                                            <h1 class="page-title">{{$type->name}}</h1> </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </header>--}}
{{--                            <div class="entry-content clearfix">--}}
{{--                                <section class="wp-block-mae-speakers-masonry speakers-masonry section">--}}
{{--                                    <div class="masonry indexed-list row">--}}
{{--                                        @foreach($type->sponsors as $sponsor)--}}
{{--                                        <div class="masonry-item indexed-list__item col-12 col-md-6 col-lg-4">--}}
{{--                                            <div class="indexed-list__in-view">--}}
{{--                                                <div class="speaker-neat">--}}
{{--                                                    <div class="speaker-neat__image poster in-view">--}}
{{--                                                        <figure><img class="lazyload--el lazyload in-view__child"--}}
{{--                                                                     alt="" width="445"--}}
{{--                                                                     data-src="{{asset('storage/'.$sponsor->image)}}"--}}
{{--                                                                     data-id="265"/>--}}
{{--                                                            <div class="poster--cover in-view in-view__child"></div>--}}
{{--                                                        </figure>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="speaker-neat__copy in-view"><h5--}}
{{--                                                            class="speaker-neat__name in-view__child in-view__child--fadein">--}}
{{--                                                            John Doe</h5>--}}
{{--                                                        <p class="speaker-neat__title sub-head-2 dashed in-view__child in-view__child--fadein">--}}
{{--                                                            Founder</p></div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                       @endforeach--}}
{{--                                    </div>--}}
{{--                                </section>--}}
{{--                            </div>--}}
{{--                        </section>--}}
{{--                    </main>--}}
{{--                </div>--}}
{{--                    @endforeach--}}
{{--        end of speaker--}}

                </div>
            </section>
        </main>
    </div>
    @endsection
@section('styles')
    <style>
       .sch{
margin-right: -10px;
       }
        .schedule{
           margin-left: -10px;
        }
    </style>

    @endsection
