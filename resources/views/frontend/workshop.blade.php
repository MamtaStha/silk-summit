@extends('frontend.layouts.front_design')
@section('content')
    <div class="content-area" id="primary">
        <main class="site-main" id="main">
            <section id="post-784" class="post-784 page type-page status-publish hentry section section--singular">
                <header class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-10 offset-xl-1">
                                <h1 class="page-title">Workshops</h1> <h5 class="page-subtitle">{{$description->workshop_subtitle}}</h5></div>
                        </div>
                    </div>
                </header>



                <div class="entry-content clearfix">
                    <div class="wp-block-pixudio-columns columns-block in-view">
                        <div class="columns columns-1 columns-md-2 columns-lg-2">
                            <div class="wp-block-pixudio-column in-view__child in-view__child--fadein">
                                <p class="dashed in-view__child">{!! $description->workshop_description !!}</p>
                            </div>
                            <div class="wp-block-pixudio-column in-view__child in-view__child--fadein"></div>
                        </div>
                    </div>



                    <section class="wp-block-mae-workshops workshops section">
                        <div class="workshops__items">
                            <div class="row">
                                @foreach($workshops as $workshop)
                                <div class="col-12 col-md-12 col-lg-6">
                                    <div class="workshop">
                                        <div class="row">
                                            <div class="col-12 col-lg-4 in-view">
                                                <div class="poster poster--square">
                                                    <figure><img class="lazyload--el lazyload in-view__child"
                                                                 src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                                                 alt="" width="445" height="445"
                                                                 data-src="{{asset('storage/'.$workshop->mentor->image)}}"
                                                                 data-id="265"/>
                                                        <div class="poster--cover in-view in-view__child"></div>
                                                    </figure>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-8 in-view"><p
                                                    class="workshop__by sub-head-2 margin-bottom-0 in-view__child in-view__child--fadein" style="font-size: large">
                                                    {{$workshop->mentor->name}}</p><h5
                                                    class="workshop__title margin-top-0 in-view__child in-view__child--fadein">
                                                    {{$workshop->title}}</h5>
                                                <p class="workshop__desc text-body-3 in-view__child in-view__child--fadein">
                                                   {!! $workshop->description !!}</p>
{{--                                                <p class="in-view__child in-view__child--fadein"><span--}}
{{--                                                        class="workshop__cta dashed dashed--hover dashed--reverse in-view__child"><strong><a--}}
{{--                                                                href="index.html#">Get it for $11.99</a></strong></span>--}}
{{--                                                </p>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach

                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </main>
    </div>
    @endsection
