@extends('frontend.layouts.front_design')
@section('content')
    <div class="content-area" id="primary">
        <main class="site-main" id="main">
            <section id="post-784" class="post-784 page type-page status-publish hentry section section--singular">
                <header class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-10 offset-xl-1">
                                <h1 class="page-title">Challenges</h1> <h5 class="page-subtitle">{{$description->challenges_subtitle}}</h5> </div>
                        </div>
                    </div>
                </header>
                <div class="entry-content clearfix">
                    <div class="wp-block-pixudio-columns columns-block in-view">
                        <div class="columns columns-1 columns-md-2 columns-lg-2">
                            <div class="wp-block-pixudio-column in-view__child in-view__child--fadein">
                                <p class="dashed in-view__child">{!! $description->challenges_description !!}</p>
                            </div>
                            <div class="wp-block-pixudio-column in-view__child in-view__child--fadein"></div>
                        </div>
                    </div>




                    <div class="entry-content clearfix">
                        @foreach($challenges as $challenge)
                        <div class="wp-block-mae-services-image section service-image  @if($loop->index==1) service-image--reverse @endif">
                            <div class="row align-items-center">
                                <div class="service-image__thumb col-12 col-lg-6">
                                    <?php  $replace = str_replace("\\", "/", $challenge->image );?>
                                    <figure class="lazyload--el lazyload" style="background-image: url({{asset('storage') . '/' .  $replace}});"
                                            data-id="840"></figure>s
                                </div>
                                <div class="service-image__copy col-12 col-lg-6 in-view"><span
                                        class="service-image__copy__subtitle d-block sub-head-2 in-view__child in-view__child--fadein">{{$challenge->sub_title}}</span>
                                    <h3 class="service-image__copy__title margin-top-0 in-view__child in-view__child--fadein">
                                        {{$challenge->title}}</h3>
                                    <div class="service-image__copy__desc indent text-body-3 in-view__child in-view__child--fadein color_div" style="color: white !important ;" >
                                   {!! $challenge->description !!} </div>
                                    <p class="indent in-view__child in-view__child--fadein"><a
                                            class="service-image__copy__cta sub-head dashed dashed--reverse dashed--hover in-view__child"
                                            href="index.html#">Get started</a></p></div>
                            </div>
                        </div>
                            @endforeach
                    </div>
                </div>
            </section>
        </main>
    </div>
    @endsection

@section('styles')
    <style>
        .color_div p span {
            color: white !important;
        }

    </style>
@endsection

