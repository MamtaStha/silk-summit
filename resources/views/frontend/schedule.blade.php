@extends('frontend.layouts.front_design')
@section('content')
    <div class="content-area" id="primary">
        <main class="site-main" id="main">
            <section id="post-782" class="post-782 page type-page status-publish hentry section section--singular">
                <header class="page-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-xl-10 offset-xl-1">
                                <h1 class="page-title">Schedule</h1> <h5 class="page-subtitle">UX design is everyone’s responsibility.</h5> </div>
                        </div>
                    </div>
                </header>


                <div class="entry-content clearfix">
                    <div class="wp-block-pixudio-schedule schedule">
                        <div class="tabs" role="tablist">
{{--                            <div class="tabs__navs">--}}
{{--                                <label class="tabs__nav tabs__nav--active" tabindex="0" data-tab="item-0">--}}
{{--                                    <span ><h1>Schedule</h1></span>--}}
{{--                                    <span class="sub-head-2"> — <span class="tabs__nav__tag">25 may</span></span></label>--}}
{{--                            </div>--}}


                            <div class="tabs__content">
                                <div class="tabs__item tabs__item--active" tabindex="0" data-tab="item-0">
                                    @foreach($schedules as $schedule)
                                    <div class="row clipcard margin-top">
                                        <div class="col-12 col-md-2 col-lg-1"><p class="clipcard__head">{{$schedule->time}}</p></div>
                                        <div class="col-12 col-md-10 col-lg-11">
                                            <div class="clipcard__body"><img
                                                    class="clipcard__thumb lazyload--el lazyload in-view__child"
                                                    data-src="{{asset('storage/'.$schedule->mentor->image)}}"
                                                    alt="" width="36" height="36"/>
                                                <p class="clipcard__info"><span
                                                        class="text-body-3">@if(!empty($schedule->duration))Duration — {{$schedule->duration}} @endif</span></p><h6
                                                    class="clipcard__title">{{$schedule->title}}</h6>
                                                <p class="clipcard__desc">{!! $schedule->description !!}</p><span class="clipcard__subtitle"><span
                                                        class="sub-head-2">@if(!empty($schedule->mentor->name))By {{$schedule->mentor->name}} @endif</span></span></div>
                                        </div>
                                    </div>
                                   @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
{{--    <div class="content-area" id="primary">--}}
{{--        <main class="site-main" id="main">--}}
{{--            <section id="post-782" class="post-782 page type-page status-publish hentry section section--singular">--}}
{{--                <header class="page-header">--}}
{{--                    <div class="container">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-12 col-xl-10 offset-xl-1">--}}
{{--                                <h1 class="page-title">Schedule</h1> <h5 class="page-subtitle">UX design is everyone’s responsibility.</h5> </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </header>--}}
{{--                <div class="entry-content clearfix">--}}


{{--                    <div class="wp-block-pixudio-schedule schedule">--}}
{{--                        <div class="tabs" role="tablist">--}}
{{--                            <div class="tabs__navs">--}}
{{--                                @foreach($days as $day)--}}
{{--                                    <label   @if($loop->index==0)class="tabs__nav  tabs__nav--active" @else class="tabs__nav"@endif tabindex="@if($loop->index==0){{$loop->index}} @else -{{$loop->index}} @endif" data-tab="item-{{$loop->index}}">--}}
{{--                                        <span class="tabs__nav__label">{{$day->name}}</span><span class="sub-head-2"> — <span class="tabs__nav__tag">{{ $day->date }}</span></span>--}}
{{--                                    </label>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                            <div class="tabs__content">--}}
{{--                                @foreach($days as $day)--}}
{{--                                <div class="tabs__item @if($loop->index==0)tabs__item--active @endif" tabindex="{{$loop->index}}" data-tab="item-{{$loop->index}}">--}}
{{--                                    @foreach($day->programs as $program)--}}
{{--                                    <div class="row clipcard @if($loop->index==0) margin-top @endif">--}}
{{--                                        <div class="col-12 col-md-2 col-lg-1"><p class="clipcard__head">{{$program->time}}</p></div>--}}
{{--                                        <div class="col-12 col-md-10 col-lg-11">--}}
{{--                                            <div class="clipcard__body"><a class="clipcard__link"--}}
{{--                                                                           href="#"></a><img--}}
{{--                                                    class="clipcard__thumb lazyload--el lazyload in-view__child"--}}
{{--                                                    data-src="{{asset('storage/'.$program->mentor->image)}}"--}}
{{--                                                    alt="" width="36" height="36"/>--}}
{{--                                                <p class="clipcard__info"><span--}}
{{--                                                        class="text-body-3">{{$program->venue}}— {{$program->duration}}</span></p><h6--}}
{{--                                                    class="clipcard__title">{{$program->title}}</h6>--}}
{{--                                                <p class="clipcard__desc">{!! $program->description !!}</p><span class="clipcard__subtitle"><span--}}
{{--                                                        class="sub-head-2">By {{$program->mentor->name}}</span></span></div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                   @endforeach--}}
{{--                                </div>--}}
{{--                                @endforeach--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </section>--}}
{{--        </main>--}}
{{--    </div>--}}
@endsection
