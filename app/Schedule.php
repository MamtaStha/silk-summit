<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Schedule extends Model
{
    public function mentor() {
        return $this->belongsTo(Mentor::class, 'mentor_id');
    }
}
