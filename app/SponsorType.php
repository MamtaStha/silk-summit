<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SponsorType extends Model
{
    public function  sponsors() {
        return $this->hasMany(Sponsor::class, 'type_id');
    }

}
