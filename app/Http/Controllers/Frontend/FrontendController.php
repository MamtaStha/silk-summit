<?php

namespace App\Http\Controllers\Frontend;

use App\About;
use App\Challenge;

use App\CommingSoon;
use App\ContactMessage;
use App\Day;
use App\Description;
use App\HomeHeader;
use App\Http\Controllers\Controller;
use App\Program;
use App\Schedule;
use App\Speaker;
use App\Sponsor;
use App\SponsorType;
use App\Venue;
use App\Workshop;
use Auth;
use App\Mail\ContactMailMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index(){
        $speaker1 =Speaker::where('order', '1')->first();
        $speaker2 =Speaker::where('order', '2')->first();
        $type =SponsorType::with('sponsors')->where('name', 'Title Sponsor')->first();
        return view('frontend.index')->with(compact('speaker1','speaker2','about','header','sponsors','description','type'));
    }
    public function about(){
        return view('frontend.about');
    }
    public function commingSoon(){
        $soon= CommingSoon::first();
        return view('frontend.comming_soon')->with(compact('soon'));
    }
    public function sponsor(){
        $sponsor_type= SponsorType::with('sponsors')->get();
        return view('frontend.sponsor')->with(compact('sponsor_type'));
    }
    public function contact(){
        return view('frontend.contact');
    }
    public function speaker(){
        $speakers=Speaker::get();
        return view('frontend.speaker')->with(compact('speakers'));
    }
    public function schedule(){
//        $programs= Program::groupBy('days_id')->get();
//        foreach($programs as $key => $program) {
//            foreach($program as $item) {
//               dd($program);
//            }
//        }

//
//        Day::with('programs')->chunk(1000, function($details) {
//
//                $products = $details->map(function($map){
//                    return $map->product;
//                })->chunk(10);
//
//                foreach($products as $chunk){
//                    AmazonProductLookup::dispatch($chunk)
//                        ->onQueue('amazon_product_lookups');
//                }
//            });





        $days= Day::get();

        $schedules=Schedule::with('mentor')->orderBy('order', 'desc')->get();
        return view('frontend.schedule')->with(compact('programs','days','header','schedules'));
    }
    public function venue(){
        $venue=Venue::first();
        return view('frontend.venue')->with(compact('venue'));
    }
    public function store(Request $request){


        $rules = [
            'name' => 'required|min:2',
            'email' => 'required|email',
            'number' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ];
        $this->validate($request, $rules);

        $message = new ContactMessage();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->number = $request->number;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->save();

         Mail::to('profitnesstore@gmail.com')->send(new ContactMailMessage($message));

         return 1;
        //return redirect(route')->with('flash_message_success','Your message have been sent sucessfully');
    }
    public function Challenge(){
        $challenges=Challenge::get();
        return view('frontend.challenge')->with(compact('challenges'));

    }
    public function Workshop(){
        $workshops =Workshop::with('mentor')->get();
        return view('frontend.workshop')->with(compact('workshops'));

    }
}
