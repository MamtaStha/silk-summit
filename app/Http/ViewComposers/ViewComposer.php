<?php


namespace App\Http\ViewComposers;


use App\About;
use App\Contact;
use App\Description;
use App\HomeHeader;
use http\Header;

class ViewComposer
{
    public function compose($view)
    {
        $contact=Contact::first();
        $header=HomeHeader::first();
        $description= Description::first();
        $about= About::first();

        $view->with(compact('contact','header','description','about'));
    }

}
