<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Program extends Model
{
    public function day() {
        return $this->belongsTo(Day::class, 'days_id');
    }
    public function mentor() {
        return $this->belongsTo(Mentor::class, 'mentor_id');
    }
}
