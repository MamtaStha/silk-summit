<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Mentor extends Model
{
    public function  workshops() {
        return $this->hasMany(Workshop::class, 'mentor_id');
    }
    public function  programs() {
        return $this->hasMany(Program::class, 'mentor_id');
    }
    public function  schedules() {
        return $this->hasMany(Schedule::class, 'mentor_id');
    }
}
