<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Day extends Model
{
    public function  programs() {
        return $this->hasMany(Program::class, 'days_id');
    }

}
